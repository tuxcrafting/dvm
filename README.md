# DVM

DVM (DiVision Machine) is a OISC (One Instruction Set Computer) with only one instruction, DOJIND (Divide Or Jump If Not Divisible) that takes two operands (a divisor and an address).

The machine only has 1 unbounded positive register.

The instruction operates as follow:

* If the specified divisor is an integer
  * If the register is divisible by the divisor
    * Divide the register by the divisor
  * Else
    * Jump to the specified address
* Else
  * Unconditionally divide the register by the divisor
  * Floor the register
  * Jump to the specified address

The address is relative (0 being the current instruction).

Dividing the register by 0 will cause the machine to halt.

## Source code format

If a line is empty it is ignored, else it is composed of 2 space-separated records:

* The first is the divisor. It can either be an integer or a fraction (numerator and denominator separated by `/`). It must be positive.
* The second is the jump address. It is an integer (can be negative, positive or zero).

## Minsky machine equivalence

Minsky machine code can be trivially converted to DVM code, proving DVM's turing completeness:

    INC <reg>
    -------
    1/P(reg) 1
    
    DJZ <reg>, <addr>
    -------------
    P(reg) addr

Where `P(n)` returns the n-th prime (1-indexed).

Note that `reg` must be greater than 0, and `addr` must be correctly adjusted for the converted code.
