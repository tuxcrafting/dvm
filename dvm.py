#!/usr/bin/python3

from fractions import Fraction
from getopt import getopt, GetoptError
from random import randint
import re
from sys import argv, stdin

class DVM:
	def __init__(self, program, debug=False):
		self.reg = 1
		self.ip = 0
		self.program = program
		self.debug = debug
	def dojind(self, divisor, address):
		if self.debug:
			print("running: (%d) %s %s %s" % (self.ip, divisor, address, factorize(self.reg)))
		if divisor == 0:
			self.running = False
			return
		if divisor.denominator == 1:
			if self.reg % divisor == 0:
				self.reg //= divisor
				self.ip += 1
			else:
				self.ip += address
		else:
			self.reg //= divisor
			self.ip += address
	def step(self):
		self.dojind(*self.program[self.ip])
	def run(self):
		self.running = True
		while self.running:
			self.step()

def parse(lines):
	for line in lines:
		if not line.strip():
			continue
		m = re.search(r"^(\d+(?:/\d+)?)\s+(-?\d+)$", line.strip())
		if m:
			yield (Fraction(m.group(1)), int(m.group(2)))
		else:
			raise SyntaxError("Invalid syntax: '%s'" % line)

PRIMES = [2, 3, 5, 7]

def get_prime(n):
	if n - 1 < len(PRIMES):
		return PRIMES[n - 1]
	n -= len(PRIMES)
	p = PRIMES[-1]
	while n:
		p += 2
		isp = True
		for i in PRIMES:
			if p % i == 0:
				isp = False
				break
		if isp:
			PRIMES.append(p)
			n -= 1
	return p

def factorize(n):
	f = []
	p = 1
	while n > 1:
		np = get_prime(p)
		f.append(0)
		while n % np == 0:
			n //= np
			f[-1] += 1
		p += 1
	return f

def extract(r, n):
	i = 0
	while r % n == 0:
		r //= n
		i += 1
	return i

def preprocess(lines):
	# first pass - expand macros
	macros = {}
	code = []
	for l in lines:
		line = re.sub(r"%.*$", "", l).strip()
		if not line:
			continue
		m = re.search(r"^@(\w+)\s+(.+)$", line)
		if m:
			macros[m.group(1)] = m.group(2).replace("$$", "\n")
		else:
			code.append(line)
	code = "\n".join(code)
	expanded = [True]
	def expand_macro(g, a=True):
		macro_random = randint(0, 2**63-1)
		expanded[0] = True
		if a:
			args = g.group(2).split(",")
		else:
			args = []
		if g.group(1) not in macros:
			if g.group(1) == "P":
				if len(args) < 1:
					raise NameError("P must have at least one argument")
				try:
					i = int(args[0])
					if i <= 0:
						raise ValueError("")
					return str(get_prime(i))
				except ValueError:
					raise ValueError("Argument must be a positive nonzero integer")
			raise NameError("Unknown macro %s" % g.group(1))
		c = macros[g.group(1)]
		def expand_var(g):
			if g.group(1) == "M":
				return str(macro_random)
			i = int(g.group(1)) - 1
			if i >= len(args) or i < 0:
				raise NameError("Invalid argument %s" % g.group(1))
			return args[i]
		return re.sub(r"\$\((\d+|M)\)", expand_var, c)
	num = 1000
	while expanded[0] and num:
		expanded[0] = False
		code = re.sub(r"(\w+)!", lambda g: expand_macro(g, False), code)
		code = re.sub(r"(\w+)\((.*?)\)", expand_macro, code)
		num -= 1
	if num == 0:
		raise Warning("Preprocessing took more than 1000 iterations")
	lines = code.split("\n")

	# second pass - expand labels
	labels = {}
	ip = 0
	code = []
	for l in lines:
		line = l.strip()
		if not line:
			continue
		m = re.search(r"^:(\w+)$", line)
		if m:
			labels[m.group(1)] = ip
		else:
			code.append((ip, line))
			ip += 1
	lines = []
	for i, l in code:
		def expand_label(g):
			if g.group(1) not in labels:
				raise NameError("Unknown label %s" % g.group(1))
			return str(labels[g.group(1)] - i)
		lines.append(re.sub("\^(\w+)", expand_label, l))
	return lines

def print_help():
	print("Usage: %s [-dpn] file [p]num=exp...")
	print("\t-d Enable debug mode")
	print("\t-p Only preprocess")
	print("\t-n Do not preprocess")

def main():
	try:
		params, args = getopt(argv[1:], "dpn")
	except GetoptError as ex:
		print(ex)
		print_help()
		return

	dopreprocess = 1
	debug = False
	for k, v in params:
		if k == "-p":
			dopreprocess = 2
		elif k == "-n":
			dopreprocess = 0
		elif k == "-d":
			debug = True

	if len(args) < 1:
		print_help()
		return

	try:
		with open(args[0]) as f:
			if dopreprocess:
				lines = preprocess(f)
				if dopreprocess == 2:
					print("\n".join(lines))
					return
			else:
				lines = list(f)
	except Exception as ex:
		print(ex)
		return

	try:
		dvm = DVM(list(parse(lines)), debug)
	except Exception as ex:
		print(ex)
		return

	for x in args[1:]:
		m = re.search("(p?\d+)=(\d+)", x)
		if m:
			if m.group(1)[0] == "p":
				a = int(m.group(1)[1:])
				if a == 0:
					print("prime index must be >= 0")
					return
				a = get_prime(a)
			else:
				a = int(m.group(1))
			b = int(m.group(2))
			dvm.reg *= a ** b
		else:
			print("invalid argument format")
			return

	dvm.run()
	for i, x in enumerate(factorize(dvm.reg)):
		n = i + 1
		if x:
			print("P(%d) [%d] = %d" % (n, get_prime(n), x))

if __name__ == "__main__":
	main()
